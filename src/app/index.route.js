(function() {
  'use strict';

  angular
    .module('angularGulp', [
      'ngAnimate',
      'ngCookies',
      'ngTouch',
      'ngSanitize',
      'ngMessages',
      'ngAria',
      'ngResource',
      'ngRoute',
      'ui.bootstrap',
      'toastr'])
    .config(function($routeProvider) {
      $routeProvider
        .when('/template', {
          templateUrl: 'app/main/template/template/list.html',
          controller: 'TemplateController',
          controllerAs: 'main'
        })
        .when('/ngrout', {
          templateUrl: 'app/main/template/ngrout/list.html',
          controller: 'NgController',
          controllerAs: 'main'
        })
        .when('/ngmodel', {
          templateUrl: 'app/main/template/ngmodel/list.html',
          controller: 'NgModelController',
          controllerAs: 'main'
        })
        .when('/skobki', {
          templateUrl: 'app/main/template/skobki/list.html',
          controller: 'NgSkobController',
          controllerAs: 'main'
        })
        .when('/ngclick', {
          templateUrl: 'app/main/template/ngclick/list.html',
          controller: 'NgClickController',
          controllerAs: 'main'
        })
        .when('/ngif', {
          templateUrl: 'app/main/template/ngif/list.html',
          controller: 'NgIfController',
          controllerAs: 'main'
        })
        .when('/ngclass', {
          templateUrl: 'app/main/template/ngclass/list.html',
          controller: 'NgClassController',
          controllerAs: 'main'
        })
        .when('/ngshhi', {
          templateUrl: 'app/main/template/ngshhi/list.html',
          controller: 'NgNgshhiController',
          controllerAs: 'main'
        })
        .when('/ngAnimate', {
          templateUrl: 'app/main/template/ngAnimate/list.html',
          controller: 'NgAnimateController',
          controllerAs: 'main'
        })
        .when('/practic1', {
          templateUrl: 'app/main/template/practic1/list.html',
          controller: 'NgPr1Controller',
          controllerAs: 'main'
        })
        .when('/form', {
          templateUrl: 'app/main/template/form/list.html',
          controller: 'FormController',
          controllerAs: 'main'
        })
        .otherwise({
          redirectTo: '/template'
        });
    });




})();
